Pod::Spec.new do |s|
  s.name         = "LaravelEchoIOS"
  s.version      = "0.1.3"
  s.summary      = "A wrapper for Laravel Echo in Swift"
  s.description  = "A wrapper for Laravel Echo with Socket.io in Swift by bubbleflat.com"
  s.homepage     = "https://github.com/val-bubbleflat/laravel-echo-ios"
  s.license      = { :type => "MIT", :file => "LICENSE"}
  s.author       = { "Valentin Vivies" => "valentin@bubbleflat.com", "bubbleflat" => "contact@bubbleflat.com" }
  s.source       = { :git => "https://gitlab.com/d_polurezov/laravel-echo-ios", :tag => "#{s.version}" }
  s.platforms = {:ios => "9.0"}
  s.source_files  = "LaravelEchoIOS/*.swift", "LaravelEchoIOS/**/*.swift"
  s.exclude_files = ""

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '5.0' }

  s.subspec 'Socket.IO-Client-Swift' do |socket|
      socket.dependency   'Socket.IO-Client-Swift'
  end

end
