//
//  EchoLogger.swift
//
//  Created by Patron Empowerment team.
//  Copyright © 2020 RhythmicRebellion. All rights reserved.
//

import os.log
import UIKit

public enum EchoLogger {
    private static var loggerDisabled = Set<Category>()
    private static var loggerEnabledOnly = Set<Category>()

    public static func disableLogging(_ category: Category ...) {
        loggerDisabled = Set(category)
    }

    public static func enableOnly(_ category: Category ...) {
        loggerEnabledOnly = Set(category)
    }

    public enum Category: String {
        case echo = "Echo"
    }

    public enum AccessLevel: String {
        case `public`
        case `private`
    }

    public static func log(
        category: EchoLogger.Category,
        message: @autoclosure () -> String,
        access: EchoLogger.AccessLevel = EchoLogger.AccessLevel.public,
        type: OSLogType = OSLogType.default
    ) {
        #if DEBUG || ADHOC
            switch access {
            case .private:
                os_log("%{private}@", log: getOSLog(category: category), type: type, message())
            case .public:
                os_log("%{public}@", log: getOSLog(category: category), type: type, message())
            }
        #endif
    }

    private static let logSubsystem = Bundle.main.bundleIdentifier ?? "-"
    private static let echo = OSLog(subsystem: logSubsystem, category: Category.echo.rawValue)

    private static func getOSLog(category: Category) -> OSLog {
        var osLog: OSLog {
            switch category {
            case .echo: return EchoLogger.echo
            }
        }
        let enabledOnly = loggerEnabledOnly
        if !enabledOnly.isEmpty, !enabledOnly.contains(category) { return OSLog.disabled }
        if loggerDisabled.contains(category) { return OSLog.disabled }
        return osLog
    }
}
