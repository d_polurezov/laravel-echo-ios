//
//  Echo.swift
//  laravel-echo-ios
//
//  Created by valentin vivies on 21/07/2017.
//

import Foundation
import SocketIO

/// This class is the primary API for interacting with broadcasting.
public class Echo {

    /// The broadcasting connector.
    public var connector: IConnector
    
    /// The Echo options.
    public var options: [String: Any]

    /// Create a new class instance.
    ///
    /// - Parameter options: options
    public init(options: [String: Any], _ isLogEnabled: Bool = false) {
        self.options = options
        // No Pusher support
        self.connector = SocketIOConnector(options: self.options, isLogEnabled)
    }

    /// when connected to the socket
    ///
    /// - Parameters:
    ///   -  onConnect: connection callback
    ///   -  onDisconnect: disconnection callback
    public func connected(
        _ isLogEnabled: Bool = false,
        onConnect: @escaping NormalCallback,
        onDisconnect: (() -> Void)? = nil
        ) {
        connector.connect(isLogEnabled, onDisconnect: onDisconnect)
        connector.on(clientEvent: .connect, callback: onConnect)
    }

    /// Catch an event
    ///
    /// - Parameters:
    ///   - event: event name
    ///   - callback: callback
    public func on(event: String, callback: @escaping NormalCallback) {
        return connector.on(event: event, callback: callback)
    }

    /// Listen for an event on a channel instance.
    ///
    /// - Parameters:
    ///   - channel: channel name
    ///   - event: event name
    ///   - callback: callback
    /// - Returns: the channel listened
    @discardableResult public func listen(channel: String, event: String, callback: @escaping NormalCallback) -> IChannel {
        return connector.listen(name: channel, event: event, callback: callback);
    }

    /// Get a channel instance by name.
    ///
    /// - Parameter channel: channel name
    /// - Returns: the channel
    @discardableResult public func channel(channel: String) -> IChannel {
        return connector.channel(name: channel)
    }

    /// Get a private channel instance by name.
    ///
    /// - Parameter channel: channel name
    /// - Returns: the private channel
    @discardableResult public func privateChannel(channel: String) -> IChannel {
        return connector.privateChannel(name: channel)
    }

    /// Get a presence channel instance by name.
    ///
    /// - Parameter channel: channel name
    /// - Returns: the private channel
    @discardableResult public func join(channel: String) -> IPresenceChannel {
        return connector.presenceChannel(name: channel)
    }

    /// Leave the given channel.
    ///
    /// - Parameter channel: the channel name
    public func leave(channel: String) {
        connector.leave(name: channel)
    }

    /// Get the Socket ID for the connection.
    ///
    /// - Returns: the socket id
    public func socketId() -> String {
        return connector.socketId()
    }

    /// Disconnect from the Echo server.
    public func disconnect() {
        connector.disconnect()
    }
}
